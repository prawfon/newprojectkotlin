package projectkotlin.demo.service

import org.springframework.stereotype.Service
import projectkotlin.demo.dao.CustomerDao
import projectkotlin.demo.entity.Customer

@Service
class CustomerServiceImpl:CustomerService {
    lateinit var customerDao: CustomerDao
    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}
