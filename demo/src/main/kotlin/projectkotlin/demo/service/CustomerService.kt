package projectkotlin.demo.service

import projectkotlin.demo.entity.Customer

interface CustomerService{
    fun getCustomers():List<Customer>
}