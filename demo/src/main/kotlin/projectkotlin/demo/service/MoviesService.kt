package projectkotlin.demo.service

import projectkotlin.demo.entity.Movies

interface MoviesService{
    fun getMovies():List<Movies>
}