package projectkotlin.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.demo.dao.MoviesDao
import projectkotlin.demo.entity.Movies

@Service
class MoviesServiceImpl:MoviesService{
    @Autowired
    lateinit var moviesDao: MoviesDao
    override fun getMovies(): List<Movies>{
        return moviesDao.getMovies()
    }
}