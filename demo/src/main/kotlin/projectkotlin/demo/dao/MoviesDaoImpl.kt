package projectkotlin.demo.dao

import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Movies

@Repository
class MoviesDaoImpl : MoviesDao {
    override fun getMovies(): List<Movies> {
        return mutableListOf(
                Movies("Dumbo",
                        "A young elephant, whose oversized ears enable him to fly, helps save a struggling circus. But when the circus plans a new venture, Dumbo and his friends discover dark secrets beneath its shiny veneer.",
                        "https://lh3.googleusercontent.com/FcKcdorsSc0A1gcPJ6Vgwjd1oVU0wgZP6m6rvOEj-INliNOeILq-ND3pR41N2RZCCP5scslco2shHqaVIq7c=w260"),
                Movies("Shazam",
                        "A boy is given the ability to become an adult superhero in times of need with a single magic word.",
                        "https://lh3.googleusercontent.com/gaToaVcS_L0LvoVdxAvdNvlm1u1-nKd03k0FhiY1t2pqzNBq0LjCEdXw8_Bumu8v7S7gdCtaf5awZUseZJ2K=w260"),
                Movies("Cadaver",
                        "When a cop who is just out of rehab takes the graveyard shift in a city hospital morgue, she faces a series of bizarre, violent events caused by an evil entity in one of the corpses.",
                        "https://lh3.googleusercontent.com/eIyJuFvni5AqQMeQhC5rvCEgJa6GcNEZtzFBI0Hv3zUxTm_RM-ZgLaasc5yvXjctAeRtDp7OdkOZBQy_u5s=w260"),
                Movies("The Kid Who Would Be King",
                        "A band of kids embark on an epic quest to thwart a medieval menace.",
                        "https://lh3.googleusercontent.com/HFfLEJM3UranRaJTy0HNa1IJQ3oGNQHtX2BTMYj7obPczafPTN5AR4LgnIuzzmEYgKH8m1MBbp1tKUG-TzWT=w260"))
    }
}