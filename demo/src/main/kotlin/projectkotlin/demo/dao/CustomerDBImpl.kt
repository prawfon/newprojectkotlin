package projectkotlin.demo.dao

import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Customer

@Repository
class CustomerDBImpl:CustomerDao {
    override fun getCustomers(): List<Customer> {
        return mutableListOf(Customer("A","a@gamil.com","123456","Apple","Banana"))
    }
}
