package projectkotlin.demo.dao

import projectkotlin.demo.entity.Customer

interface CustomerDao{
    fun getCustomers():List<Customer>
}