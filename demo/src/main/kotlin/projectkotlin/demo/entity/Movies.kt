package projectkotlin.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class Movies(var name:String,
                  var description:String,
                  var imageUrl: String)
{
    @Id
    @GeneratedValue
    var id:Long? = null

}