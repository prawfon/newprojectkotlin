package projectkotlin.demo.entity

import java.util.*
import javax.persistence.*

@Entity
data class Booking(
                   var date: Date)
{
    @Id
    @GeneratedValue
    var id:Long? = null

//    @ManyToMany
//    var seats :Seats? = null
    @OneToMany
    var customer = mutableListOf<Customer>()
}