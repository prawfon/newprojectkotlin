package projectkotlin.demo.entity

import java.time.chrono.ChronoLocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany
@Entity
data class Seats(var seatnumber:Int,
                 var typeseat:String,
                 var price: Double)
{
    @Id
    @GeneratedValue
    var id:Long? = null
    @ManyToMany
    var showtime = mutableListOf<Showtime>()
}