package projectkotlin.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Customer(override var username: String,
                    override var email: String,
                    override var password: String,
                    override  var firstname:String,
                    override var lastname:String
) :User{
    @Id
    @GeneratedValue
    var id: Long? = null
}