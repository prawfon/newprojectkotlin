package projectkotlin.demo.entity

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
data class Showtime(var stardate: Date,
                    var enddate:Date)
{
    @Id
    @GeneratedValue
    var id:Long? = null

    @ManyToMany
    var movies = mutableListOf<Movies>()
}