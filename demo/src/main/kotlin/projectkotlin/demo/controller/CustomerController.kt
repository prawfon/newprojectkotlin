package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import projectkotlin.demo.service.CustomerService

@RestController
class CustomerController{
    @Autowired
    lateinit var customerService: CustomerService
    @GetMapping("/customer")
    fun getAllCustomer():ResponseEntity<Any>{
        return ResponseEntity.ok(customerService.getCustomers())
    }
}