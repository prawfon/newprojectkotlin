package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import projectkotlin.demo.service.MoviesService

@RestController
class MoviesController{
   @Autowired
    lateinit var moviesService: MoviesService
    @GetMapping("/Movies")
    fun getAllMovies():ResponseEntity<Any>{
        return  ResponseEntity.ok(moviesService.getMovies())
    }
}
//@RestController
//class MoviesController{
//    @Autowired
//    lateinit var moviesService: MoviesService
//    @GetMapping("/Movies")
//    fun getAllMovies():ResponseEntity<Any>{
//        val movie = Movies("Dumbo",
//                "A young elephant, whose oversized ears enable him to fly, helps save a struggling circus. But when the circus plans a new venture, Dumbo and his friends discover dark secrets beneath its shiny veneer.",
//                "https://lh3.googleusercontent.com/FcKcdorsSc0A1gcPJ6Vgwjd1oVU0wgZP6m6rvOEj-INliNOeILq-ND3pR41N2RZCCP5scslco2shHqaVIq7c=w260")
//        return  ResponseEntity.ok(movie);
//    }
//}